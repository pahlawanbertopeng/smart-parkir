/*
 * The MIT License
 *
 * Copyright 2014 elan.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package smartparking.utils;

import java.io.FileInputStream;
import java.lang.NullPointerException;
import java.util.Properties;

/**
 *
 * @author elan, ian
 * 
 */
public class ServerSettings {
    private static ServerSettings instance;
    private Properties prop;
    private final String FILE_NAME = "btp_smart_parking.properties";
    
    public static final String SETTING_NAME = "btp_rfid";
    public static final String PROP_DBNAME = "dbname";
    public static final String PROP_DBPATH = "dbpath";
    public static final String PROP_DBUNAME = "dbuname";
    public static final String PROP_DBPASS = "dbpwd";
    public static final String PROP_PROGRAM = "program";    
    public static final String PROP_SERVER_IP ="serverIP";
    public static final String PROP_SERVER_PORT = "serverPort";
    public static final String PROP_MAX_CONNECTION ="maximumconnection";
    public static final String PROP_VERBOSE_MODE = "verbose";
    public static final String PROP_DB_HOST = "dbHost";
    public static final String PROP_DB_DATASOURCE = "dataSource";
    
    
    public static final int ALLOW_EMERGENCY_REQUEST = 1;
    
    private ServerSettings() throws Exception
    {
        prop = new Properties();
        prop.load(new FileInputStream(FILE_NAME));        
    }
    
    public static ServerSettings getDefault() throws Exception
    {
        if(instance==null)
        {
            instance = new ServerSettings();
        }
        return instance;
    }
    
    public Properties getProperties() throws Exception
    {
        if(prop!=null){
            return prop;
        }else{
            getDefault();
            return prop;
        }  
    }
    
    public static void setPropertyValue(ServerSettings setting, String key, String value) throws Exception
    {
        if(setting!=null)
        {
            Properties prop = setting.getProperties();
            prop.put(key, value);
        }else{
            throw new NullPointerException();
        }
    }
    
     
    public static String getPropertyValue(ServerSettings setting, String key) throws Exception
    {
        String value = "";
        if(setting!=null && key!=null)
        {
            Properties prop = setting.getProperties();
            value = prop.getProperty(key);
        }
        return value;
    }
    
    public static boolean isVerbose(ServerSettings setting) throws Exception
    {
        if(setting==null)
        {            
            throw new NullPointerException();
        }else{
            int stat = Integer.parseInt(ServerSettings.getPropertyValue(setting, PROP_VERBOSE_MODE));
            if(stat==0)
            {
                return false;
            }
        }
        return true;
    }
}