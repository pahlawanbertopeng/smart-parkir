/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package smartparking.utils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 *
 * @author elan
 */
public class Serializer {
    public static byte[] serialize(Object obj) throws IOException
    {
        ByteArrayOutputStream arrayOutput = new ByteArrayOutputStream();
        ObjectOutputStream outputStream = new ObjectOutputStream(arrayOutput);
        outputStream.writeObject(obj);
        return arrayOutput.toByteArray();
    }
    
    public static Object deserialize(byte[] bytes) throws IOException,ClassNotFoundException
    {
        ByteArrayInputStream arrayInput = new ByteArrayInputStream(bytes);
        ObjectInputStream inputStream = new ObjectInputStream(arrayInput);
        return inputStream.readObject();
    }
//Read data from socket
//InputStream stream = socket.getInputStream();
//byte[] data = new byte[100];
//int count = stream.read(data);    
}
