/*
 * The MIT License
 *
 * Copyright 2014 elan.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package smartparking.utils;

import java.util.logging.FileHandler;
import java.util.logging.Formatter;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;
import org.joda.time.DateTime;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import smartparking.Init;
import smartparking.db.ConnectDB;
import smartparking.db.model.LogData;

/**
 *
 * @author elan
 */
public class Logging implements Job
{
    public static final String DATE_FORMAT = "yyyy-MM-dd HH-mm";        
    public static final String HOUR_FORMAT = "HH:mm";        

    private final static Logger logger = Logger.getLogger("btp_rfid_log");
    private FileHandler fh;    
    private String filename;
    private Logging instance;
    
    public Logging()
    {
        
    }   

    public Logging getInstance() {
        return instance;
    }
        
    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }
        
    
    /**
     * Inisiasi dari class logging. sebelum memanggil method ini, sebaiknya tentukan dulu
     * nama file log yang ingin disimpan dengan memanggil method setFilename(String filename)
     * @throws Exception 
     */
    public void init() throws Exception
    {
        // Remove all possible handler in logger instance        
        for(Handler h: logger.getHandlers())
        {
            logger.removeHandler(h);
        }
        fh = new FileHandler(getFilename(),true);
        fh.setFormatter(new Formatter()
        {

            @Override
            public String format(LogRecord record) {
                return record.getLevel()+" : "+record.getMessage()+"\n";
            }
            
        });        
        logger.setLevel(Level.CONFIG);
        logger.addHandler(fh);        
    }        
    /**
     * Menulis pesan kedalam file yang telah diinstansiasi di logger
     * @param message - pesan yang ingin disimpan kedalam log file
     * @param level - prioritas dari pesan
     */
    public void log(String message,Level level)
    {
        logger.log(level,message);       
    }
    
    /**
     * Tulis log kedalam database
     * @param log - data log
     */
    public void logToDB(LogData log)
    {
        ConnectDB con = Init.getDBConnectionInstance();        
        LogData logdata = new LogData();
        Logging logging = Init.getLoggerInstance();        
        try {            
            LogData.insertLog(con,log);            
            logdata.setLogMessage("Log successfully saved to database");
            logdata.setLogtime(new DateTime().toString(DATE_FORMAT));            
            
            logging.log(logdata.toString(), Level.INFO);
        } catch (Exception ex) {           
           logdata.setLogMessage(ex.getMessage());
           logdata.setLogtime(new DateTime().toString(DATE_FORMAT));
                      
           logging.log(logdata.toString(), Level.WARNING);
        }
    }
        
    /**
     * Eksekusi penjadwalan untuk membuat file log baru ketika sudah mencapai hari baru di jam 12 AM
     * @param jec
     * @throws JobExecutionException 
     */
    @Override
    public void execute(JobExecutionContext jec) throws JobExecutionException 
    {
        // Get the Log data first        
        JobDataMap data = jec.getJobDetail().getJobDataMap();
        ServerSettings settings = (ServerSettings)data.get(ServerSettings.SETTING_NAME);                                        
                
        String programName;
        LogData logdata = new LogData();
        try {
            Logging log = new Logging();
            programName = LogData.getKindString(Integer.parseInt(settings.getProperties().getProperty(ServerSettings.PROP_PROGRAM)));
            log.setFilename(new DateTime().toString(Logging.DATE_FORMAT)+" - btp -"+ programName+"- log.log");
            log.init();
            
            // Write success message to log
            logdata.setLevel(Level.INFO);            
            logdata.setLogtime(new DateTime().toString(DATE_FORMAT));
            logdata.setLogMessage(log.getFilename()+" file is created");            
            log.log(logdata.toString(), Level.INFO);
            
            // Change logger instance to the new one            
//            System.out.println("Before set instance : "+Init.getLoggerInstance().toString());
            Init.setLoggerInstance(log);
//            System.out.println("After set instance : "+Init.getLoggerInstance().toString());
        } catch (Exception ex) {
            // Write Error log file                                    
            logdata.setLogtime(new DateTime( ).toString(DATE_FORMAT));
            logdata.setLogMessage(ex.getMessage());
            
            Logging logging = Init.getLoggerInstance();
            logging.log(logdata.toString(), Level.WARNING);
        }                       
    }
       
}
        
