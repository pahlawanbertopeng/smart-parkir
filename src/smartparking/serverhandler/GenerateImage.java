/*
 * The MIT License
 *
 * Copyright 2014 ian.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package smartparking.serverhandler;

import java.awt.image.BufferedImage;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
//import java.net.MalformedURLException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.URL;
import java.util.Iterator;
import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.stream.ImageOutputStream;

/**
 *
 * @author ian
 */
public class GenerateImage {
    
    private String direccionIP = "";
    private String pathImg = "";
    private String imageName = "default";
    public BufferedImage frame = null;
    
    GenerateImage(String ip)
    {
        direccionIP = ip;
    }
    /*
     * Used to compress image to jpg
     */
    private void compressImage() throws IOException        
    {
        File outputfile = new File(pathImg + "/" + imageName + ".jpg");        
        OutputStream os = new FileOutputStream(outputfile);
        
        Iterator<ImageWriter> writers = ImageIO.getImageWritersByFormatName("jpg");
        ImageWriter writer = (ImageWriter) writers.next();
        
        ImageOutputStream ios = ImageIO.createImageOutputStream(os);
        writer.setOutput(ios);
        
        //set image generation parameter
        ImageWriteParam param = writer.getDefaultWriteParam();
        param.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
        param.setCompressionQuality(0.5f);        
        writer.write(null, new IIOImage(frame, null, null), param);
        
        //close the stream
        os.close();
        ios.close();
        writer.dispose();
    }

    public void getImage()
    {
        try{
            URL nUrl = new URL(direccionIP);
            frame = ImageIO.read(nUrl);
            if (frame != null) {
                File outputfile = new File(pathImg + "/" + imageName + ".png");
                //ImageIO.write(frame, "png", outputfile);
                compressImage();
            }

            } catch (IOException ex) {
                //hayConexion = false;
                //g2.drawString(ex.toString(), 5, 15);
                try(PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("error-log.txt", true)))) {
                        //out.println(DateTime.now()+" : error "+ex.getMessage());
                        ex.printStackTrace();
                        ex.printStackTrace(out);
                    }catch (IOException e) {
                        //exception handling left as an exercise for the reader
                    }
            }
    }

    public String getDireccionIP() {
        return direccionIP;
    }

    public BufferedImage getFrame() {
        return frame;
    }

    public void setDireccionIP(String direccionIP) {
        this.direccionIP = direccionIP;
    }
    
    public String getPathImg() {
        return pathImg;
    }

    public void setPathImg(String pathImg) {
        this.pathImg = pathImg;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }
}
