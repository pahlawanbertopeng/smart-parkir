/*
 * The MIT License
 *
 * Copyright 2014 ian.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package smartparking.serverhandler;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.joda.time.DateTime;
import smartparking.Init;
import smartparking.db.model.RequestData;
import smartparking.db.model.Card;
import smartparking.db.ConnectDB;
import smartparking.db.model.CaptureEvent;
import smartparking.db.model.IpCamList;
import smartparking.db.model.LogData;
import smartparking.db.model.ResponseData;
import smartparking.db.model.Room;
import smartparking.db.model.User;
import smartparking.serverlistener.ServerListener;
import smartparking.utils.Logging;

/**
 *
 * @author ian
 */
public class ServerHandler {
    private Card card;
    private ConnectDB conn;
    private ServerListener sl;
    private String sa;
    private String ipClient;
    
    public ServerHandler() throws Exception
    {
        conn = ConnectDB.getDefault(); 
        //sl = Init.getServerListener();
    }
    
    public String getSa() {
        return sa;
    }

    public void setSa(String sa) {
        this.sa = sa;
    }
    
    public synchronized byte[] processRequest(RequestData Rdata,String ipClient)
    {        
        this.ipClient = ipClient;
        if(Rdata.getClientMessage().contains("emergency"))
        {
            return emergencyLog(Rdata);
        }
        else
        {
            if(Rdata.getClientType() == 1) {
                return checkParking(Rdata);
            } else {
                return checkPresence(Rdata);
            }
        }
    }
    
    private byte[] emergencyLog(RequestData Rdata)
    {
        ResponseData rd = new ResponseData();
        rd.setIsCard(0);
        rd.setIsRegister(0);
        rd.setIsOpen(0);
        rd.setDatetime(new DateTime().toString(Logging.DATE_FORMAT));
        rd.setUserId("0000000000");
        rd.setUsername("0000000000");
        rd.setDescription("0000000000");
        LogData logdata = new LogData();
        logdata.setLogIp(sa.toString());
        logdata.setLogtime(new DateTime().toString(Logging.DATE_FORMAT));
        try {
            LogData.insertLogEmergency(conn, logdata);
            return rd.toBytes();
        } catch (Exception ex) {
            Logger.getLogger(ServerHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }
    
    private byte[] checkPresence(RequestData Rdata)
    {
        ResponseData rd = new ResponseData();
        try {
            Card crd = Card.findById(conn, Rdata.getCardId());
            if(crd == null)
            {
                rd.setIsCard(0);
                rd.setIsRegister(0);
                rd.setIsOpen(0);
                rd.setDatetime(new DateTime().toString(Logging.DATE_FORMAT));
                rd.setUserId("0000000000");
                rd.setUsername("0000000000");
                rd.setDescription("0000000000");
            }
            else
            {
                User usr = Card.getCardRegistered(conn, Rdata.getCardId());
                if(usr == null)
                {
                    rd.setIsCard(1);
                    rd.setIsRegister(0);
                    rd.setIsOpen(0);
                    rd.setDatetime(new DateTime().toString(Logging.DATE_FORMAT));
                    rd.setUserId("0000000000");
                    rd.setUsername("0000000000");
                    rd.setDescription("0000000000");
                }
                if(!Room.isAuthorized(conn, ipClient, usr))
                    {
                        
                    rd.setIsCard(1);
                    rd.setIsRegister(1);
                    rd.setIsOpen(0);
                    rd.setDatetime(new DateTime().toString(Logging.DATE_FORMAT));
                    rd.setUserId("0000000000");
                    rd.setUsername("0000000000");
                    rd.setDescription("0000000000");
                    
                    }
                else
                {
                    rd.setIsCard(1);
                    rd.setIsRegister(1);
                    rd.setIsOpen(1);
                    rd.setDatetime(new DateTime().toString(Logging.DATE_FORMAT));
                    rd.setUserId(verifiedString(String.valueOf(usr.getuId())));
                    rd.setUsername(verifiedString(usr.getuName()));
                    rd.setDescription(usr.getuDesc());
                    
                    Logging log = Init.getLoggerInstance();
                    LogData logdata = new LogData();
                    logdata.setLevel(Level.INFO);
                    logdata.setLogKind(2);
                    logdata.setLogIp(sa.toString());
                    logdata.setLogMessage("berhasil masuk");
                    logdata.setLogtime(new DateTime().toString(Logging.DATE_FORMAT));
                    logdata.setCard(crd);
                    log.logToDB(logdata);
                }
            }
            return rd.toBytes();
        } catch (Exception ex) {
            Logger.getLogger(ServerHandler.class.getName()).log(Level.SEVERE, null, ex.getMessage());
        }
        
        return null;
    }
    
    private byte[] checkParking(RequestData Rdata)
    {
        ResponseData rd = new ResponseData();
        try {
            Card crd = Card.findById(conn, Rdata.getCardId());
            if(crd == null)
            {
                rd.setIsCard(0);
                rd.setIsRegister(0);
                rd.setIsOpen(0);
                rd.setDatetime(new DateTime().toString(Logging.DATE_FORMAT));
                rd.setUserId("0000000000");
                rd.setUsername("0000000000");
                rd.setDescription("0000000000");
            }
            else
            {
                User usr = Card.getCardRegistered(conn, Rdata.getCardId());
                if(usr == null)
                {
                    rd.setIsCard(1);
                    rd.setIsRegister(0);
                    rd.setIsOpen(0);
                    rd.setDatetime(new DateTime().toString(Logging.DATE_FORMAT));
                    rd.setUserId("0000000000");
                    rd.setUsername("0000000000");
                    rd.setDescription("0000000000");
                }
                else if(usr!=null)
                {
                                        
                        rd.setIsCard(1);
                        rd.setIsRegister(1);
                        rd.setIsOpen(1);
                        rd.setDatetime(new DateTime().toString(Logging.DATE_FORMAT));
                        rd.setUserId(verifiedString(String.valueOf(usr.getuId())));
                        rd.setUsername(verifiedString(usr.getuName()));
                        rd.setDescription(usr.getuDesc());


                        Logging log = Init.getLoggerInstance();
                        LogData logdata = new LogData();
                        logdata.setLevel(Level.INFO);
                        logdata.setLogKind(1);
                        logdata.setLogIp(sa.toString());
                        logdata.setLogMessage("berhasil masuk");
                        logdata.setLogtime(new DateTime().toString(Logging.DATE_FORMAT));
                        logdata.setCard(crd);

                        LogData.insertLog(conn, logdata);

                        if(Rdata.getIsCapture() == 1)
                        {
                            IpCamList camUrl = IpCamList.findByIp(conn, sa);
                            if(camUrl != null)
                            {
                                GenerateImage gi = new GenerateImage(camUrl.getuUrl());//alamat generate ke IP cam
                                gi.setPathImg("log_capture_parking");
                                String imagename = usr.getuId()+this.generateImgName();
                                gi.setImageName(imagename);

                                gi.getImage();

                                CaptureEvent ce = new CaptureEvent();
                                ce.setCardid(Rdata.getCardId());
                                ce.setImageid("log_capture_parking/"+imagename+".jpg");

                                boolean checking = ce.insertLogImage(conn, ce);
                            }
                        }
                    
                }                                
            }
            return rd.toBytes();
        } catch (Exception ex) {
            try {
                Logging log = Init.getLoggerInstance();
                log.log(ex.getMessage(), Level.WARNING);
                ex.printStackTrace(new PrintWriter(new BufferedWriter(new FileWriter("error-log.txt", true))));
                //Logger.getLogger(ServerHandler.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex1) {
                //Logger.getLogger(ServerHandler.class.getName()).log(Level.SEVERE, null, ex1);
            }
        }
        return null;
    }
    
    private String generateImgName()
    {
        String generate = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());        
        return generate;
    }
    
    private String verifiedString(String varString)
    {
        /*if(varString.length() < 11)
        {
            while(varString.length() < 11)
            {
                varString += "0";
            }
        }
        else
        {
            varString = varString.substring(0, 10);
        }*/
        
        return varString;
    }

}
