/*
 * The MIT License
 *
 * Copyright 2014 elan.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package smartparking.db.model;

import java.io.ByteArrayInputStream;
import java.util.Arrays;
import org.apache.commons.io.*;
//import org.apache.commons.io.IOUtils;

/**
 *
 * @author elan
 */
public class RequestData{
    private int clientType;
    private String cardId;
    private int isCapture;
    private String clientMessage;
    private boolean validMsg = true;
    private String smsgClientFull;
    
    public RequestData()
    {
        
    }
    
    public RequestData(byte[] buffer)
    {
        byte[] clientType;
        byte[] cardId;
        byte[] isCapture;
        byte[] clientMessage;
        
        clientType = Arrays.copyOfRange(buffer, 0, 1);
        cardId = Arrays.copyOfRange(buffer, 1, 9);
        isCapture = Arrays.copyOfRange(buffer, 9, 10);
        clientMessage =Arrays.copyOfRange(buffer, 10, 19);
        
        int i;
        String sClientType="";
        String scardId="";
        String sisCapture="";
        String sclientMessage="";
        
        ByteArrayInputStream strClientType = new ByteArrayInputStream(clientType);                                
        ByteArrayInputStream strCardId = new ByteArrayInputStream(cardId);                                
        ByteArrayInputStream strIsCapture = new ByteArrayInputStream(isCapture);                                
        ByteArrayInputStream strClientMessage = new ByteArrayInputStream(clientMessage);                                
        
        while((i=strClientType.read())!=-1){            
            sClientType+=(char)i;
        }
        while((i=strCardId.read())!=-1){            
            scardId+=(char)i;
        }
        while((i=strIsCapture.read())!=-1){            
            sisCapture+=(char)i;
        }
        while((i=strClientMessage.read())!=-1){            
            sclientMessage+=(char)i;
        }
        
        this.cardId=scardId;
        this.clientMessage=sclientMessage;
        this.clientType=Integer.parseInt(sClientType);
        this.isCapture=Integer.parseInt(sisCapture);
        
        IOUtils.closeQuietly(strCardId);
        IOUtils.closeQuietly(strClientMessage);
        IOUtils.closeQuietly(strClientType);
        IOUtils.closeQuietly(strIsCapture);
    }
    
    public RequestData(String msgClientFull)
    {                
        String msgList[];        
        if(msgClientFull!=null)
        {
            msgList = msgClientFull.split("#");
        
        if(msgList.length < 3)
        {
            this.validMsg = false;
        }
        else
        {
            int idLength = msgList[1].length();
            if(msgList[0].length() >1)
                this.validMsg = false;
            else
                this.clientType=Integer.parseInt(msgList[0]);
            if(idLength == 14 || idLength == 8 )
                this.cardId=msgList[1];
            else
                this.validMsg = false;                
            if(msgList[2].length() >1)
                this.validMsg = false;
            else
                this.isCapture=Integer.parseInt(msgList[2]);
            if(msgList.length == 4)
                this.clientMessage=msgList[3];
        }
        }        
    }
    
    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public String getClientMessage() {
        return clientMessage;
    }

    public void setClientMessage(String clientMessage) {
        this.clientMessage = clientMessage;
    }

    public int getClientType() {
        return clientType;
    }

    public void setClientType(int clientType) {
        this.clientType = clientType;
    }

    public int getIsCapture() {
        return isCapture;
    }

    public void setIsCapture(int isCapture) {
        this.isCapture = isCapture;
    }   

    public boolean isValidMsg() {
        return validMsg;
    }

    public void setValidMsg(boolean validMsg) {
        this.validMsg = validMsg;
    }
    
    public String toString() {
        return "RequestData{" + "clientType=" + clientType + ", cardId=" + cardId + ", isCapture=" + isCapture + ", clientMessage=" + clientMessage + '}';
    }
    
    public int length()
    {        
        return smsgClientFull.length();
    }
    
    
}
