/*
 * The MIT License
 *
 * Copyright 2014 ian.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package smartparking.db.model;

import smartparking.db.ConnectDB;

/**
 *
 * @author ian
 */
public class IpCamList 
{
    public static final String DB_NAME = "btp_rfid_list_ipcam";
    public static final String IP = "ip";
    public static final String URL = "url";
        
    private String uIp;
    private String uUrl;

    public String getuIp() {
        return uIp;
    }

    public void setuIp(String uIp) {
        this.uIp = uIp;
    }

    public String getuUrl() {
        return uUrl;
    }

    public void setuUrl(String uUrl) {
        this.uUrl = uUrl;
    }
    /**
     * Dapatkan data room berdasarkan ID
     * @param con
     * @param ip
     * @return
     * @throws Exception 
     */
    public static IpCamList findByIp(ConnectDB con,String ip) throws Exception
    {
        IpCamList camList = null;
        String sql = "SELECT * FROM "+DB_NAME+" WHERE "+IP+"='"+ip+"'";
        con.seleksi(sql);
        if(con.getNext())
        {
            camList = new IpCamList();
            camList.setuIp(con.getText(IP));
            camList.setuUrl(con.getText(URL)); 
            
        }
        return camList;
    }
}
