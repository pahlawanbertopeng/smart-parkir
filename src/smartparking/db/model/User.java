/*
 * The MIT License
 *
 * Copyright 2014 elan.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package smartparking.db.model;

import java.util.ArrayList;
import java.util.List;
import smartparking.db.ConnectDB;

/**
 *
 * @author elan
 */
public class User {
    public static final String DB_NAME = "btp_rfid_master_user";
    public static final String USER_ID = "user_id";
    public static final String CARD_ID = "card_id";    
    public static final String USER_NAME = "user_name";
    public static final String USER_PHONE = "user_phone";
    public static final String USER_EMAIL = "user_email";
    public static final String USER_DESC = "user_description";    
    
            
    private int uId;
    private String uName;
    private String uMail;
    private String uPhone;
    private String uDesc;
    
    private List<Room> uRooms;
    private Card uCards;

    public Card getuCards() {
        return uCards;
    }

    public void setuCards(Card uCards) {
        this.uCards = uCards;
    }

    public String getuDesc() {
        return uDesc;
    }

    public void setuDesc(String uDesc) {
        this.uDesc = uDesc;
    }

    public int getuId() {
        return uId;
    }

    public void setuId(int uId) {
        this.uId = uId;
    }

    public String getuMail() {
        return uMail;
    }

    public void setuMail(String uMail) {
        this.uMail = uMail;
    }

    public String getuName() {
        return uName;
    }

    public void setuName(String uName) {
        this.uName = uName;
    }

    public String getuPhone() {
        return uPhone;
    }

    public void setuPhone(String uPhone) {
        this.uPhone = uPhone;
    }

    public List<Room> getuRooms() {
        return uRooms;
    }

    public void setuRooms(List<Room> uRooms) {
        this.uRooms = uRooms;
    }
    /**
     * Tampilkan data user secara keseluruhan
     * @param con
     * @return
     * @throws Exception 
     */
    public static List<User> listUsers(ConnectDB con) throws Exception
    {        
        List<User> users = new ArrayList<>();
        String sql = "SELECT * FROM "+DB_NAME;
        con.seleksi(sql);
        while(con.getNext())
        {
            User user = new User();
            user.setuDesc(con.getText(USER_DESC));
            user.setuId(Integer.parseInt(con.getText(USER_ID)));
            user.setuMail(con.getText(USER_EMAIL));
            user.setuName(con.getText(USER_NAME));
            user.setuPhone(con.getText(USER_PHONE));
            user.setuCards(Card.getUserCard(con, user.getuId()));
            user.setuRooms(Room.userRoomList(con, user.getuId()));
            
            users.add(user);
        }
        return users;
    }
    
    /**
     * Tampilkan data user berdasarkan Id
     * @param con
     * @param uId
     * @return
     * @throws Exception 
     */
    public static User findById(ConnectDB con, int uId) throws Exception
    {
        User user = null;
        String sql = "SELECT * FROM "+DB_NAME+" "
                + "WHERE "+USER_ID+"="+uId;
        if(con.getNext())
        {
            user = new User();
            
            user.setuDesc(con.getText(USER_DESC));
            user.setuId(Integer.parseInt(con.getText(USER_ID)));
            user.setuMail(con.getText(USER_EMAIL));
            user.setuName(con.getText(USER_NAME));
            user.setuPhone(con.getText(USER_PHONE));
            user.setuCards(Card.getUserCard(con, user.getuId()));
            user.setuRooms(Room.userRoomList(con, user.getuId()));
            
        }
        return user;
    }
}
