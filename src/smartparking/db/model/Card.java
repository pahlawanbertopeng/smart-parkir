/*
 * The MIT License
 *
 * Copyright 2014 elan.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package smartparking.db.model;

import java.util.ArrayList;
import java.util.List;
import smartparking.db.ConnectDB;

/**
 *
 * @author elan
 */
public class Card {
    public static final String DB_NAME="btp_rfid_master_card";
    public static final String CARD_IMAGE_TRANSACTION = "btp_rfid_parking_image";
    public static final String CARD_ID = "card_id";
    public static final String CARD_DESC = "card_description";
    public static final String IMAGE_ID = "image_id";
    
    private String cId;
    private String cDesc;    
    private int imageId;

    public String getcId() {
        return cId;
    }

    public void setcId(String cId) {
        this.cId = cId;
    }

    public String getcDesc() {
        return cDesc;
    }

    public void setcDesc(String cDesc) {
        this.cDesc = cDesc;
    }

    public int getImageId() {
        return imageId;
    }

    public void setImageId(int imageId) {
        this.imageId = imageId;
    }
    /**
     * Mendapatkan card berdasarkan ID
     * @param con
     * @return
     * @throws Exception 
     */
    public static Card findById(ConnectDB con,String cardId) throws Exception
    {
        Card card = null;
        String sql = "SELECT * FROM "+DB_NAME+" WHERE "+CARD_ID+"= '"+cardId+"'";
        con.seleksi(sql);
        if(con.getNext())
        {
            card = new Card();
            card.setcId(cardId);
            if(con.getText(CARD_DESC)!=null)
            {
                card.setcDesc(con.getText(CARD_DESC));
            }else{
                card.setcDesc(null);
            }            
        }
        return card;
        
    }
    
    /**
     * Mendapatkan list card secara keseluruhan
     * @param con
     * @return
     * @throws Exception 
     */
    public static List<Card> listCards(ConnectDB con) throws Exception
    {
        List<Card> cards = new ArrayList<>();
        String sql = "SELECT * FROM "+DB_NAME;
        con.seleksi(sql);
        while(con.getNext())
        {
            Card card = new Card();
            card.setcId(con.getText(CARD_ID));
            card.setcDesc(con.getText(CARD_DESC));
            
            cards.add(card);
        }
        return cards;
    }
    /**
     * Mendapatkan data kartu yang dimiliki user tertentu
     * @param con
     * @param uid
     * @return
     * @throws Exception 
     */
    public static Card getUserCard(ConnectDB con,int uid) throws Exception
    {       
        Card card = null;
        String data = "c."+CARD_ID+",c."+CARD_DESC;
        String sql = "SELECT "+data+ "FROM "+DB_NAME+" c "
                + "JOIN "+User.DB_NAME+" u "
                + "ON (c."+CARD_ID+"=u."+User.CARD_ID+") "
                + "WHERE u."+User.USER_ID+"="+uid;
        con.seleksi(sql);
        if(con.getNext())
        {
           card = new Card();
           card.setcId(con.getText(CARD_ID));
           card.setcDesc(con.getText(CARD_DESC));
        }
        return card;
    }
    
    public static User getCardRegistered(ConnectDB con, String cardid) throws Exception
    {
        User user = null;
        String query = "SELECT user_id, user_name, user_description " +
                "FROM btp_rfid_master_user WHERE card_id ='" + cardid+"'";
        con.seleksi(query);        
        if(con.getNext())
        {
            user = new User();
            
                user.setuId(Integer.valueOf(con.getText("user_id")));
            
            user.setuName(con.getText("user_name"));
            user.setuDesc(con.getText("user_description"));
        }
        return user;
    }
}
