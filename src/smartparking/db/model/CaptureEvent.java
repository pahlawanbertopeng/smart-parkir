/*
 * The MIT License
 *
 * Copyright 2014 ian.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package smartparking.db.model;

import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import smartparking.db.ConnectDB;

/**
 *
 * @author ian
 */
public class CaptureEvent {
    public static final String TBL_PARKING_IMAGE = "btp_rfid_parking_image";
    public static final String IMAGE_ID = "image_id";
    public static final String CARD_ID = "card_id";
    
    private String imageid;
    private String cardid;
    
    public CaptureEvent()
    {
        imageid = null;
        cardid = null;
    }
    
    public String getImageid() {
        return imageid;
    }

    public void setImageid(String imageid) {
        this.imageid = imageid;
    }

    public String getCardid() {
        return cardid;
    }

    public void setCardid(String cardid) {
        this.cardid = cardid;
    }
    
    public boolean insertLogImage(ConnectDB con,CaptureEvent ce)
    {
        String sql = "INSERT INTO "+TBL_PARKING_IMAGE+ "("+IMAGE_ID+","+CARD_ID+") "
                + "VALUES (\""+ ce.imageid+"\",\""+ce.cardid+"\")";
        try {
            con.updateQuery(sql);
        } catch (SQLException ex) {
            Logger.getLogger(CaptureEvent.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return true;
    }
}
