/*
 * The MIT License
 *
 * Copyright 2014 elan.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package smartparking.db.model;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import smartparking.db.ConnectDB;

/**
 *
 * @author elan, ian
 */
public class LogData {
    public static final String DB_PRESENCE = "btp_rfid_presence_log";
    public static final String DB_PARKING = "btp_rfid_parking_log";
    public static final String LOG_TIME = "log_time";
    public static final String CARD_ID = "card_id";
    public static final String LOG_IP = "log_ip";
    public static final String CLIENT_MSG = "client_message";
    
    public static final int PARKING_LOG = 1;
    public static final int PRESENCE_LOG = 2;

    private String logtime;
    private Card card;
    private String logIp;
    private String logMessage;
    private int logKind;
    private Level level;

    public Level getLevel() {
        return level;
    }

    public void setLevel(Level level) {
        this.level = level;
    }
        

    public Card getCard() {
        return card;
    }

    public void setCard(Card card) {
        this.card = card;
    }

    public String getLogIp() {
        return logIp;
    }

    public void setLogIp(String logIp) {
        this.logIp = logIp;
    }

    public int getLogKind() {
        return logKind;
    }

    public void setLogKind(int logKind) {
        this.logKind = logKind;
    }

    public String getLogMessage() {
        return logMessage;
    }

    public void setLogMessage(String logMessage) {
        this.logMessage = logMessage;
    }

    public String getLogtime() {
        return logtime;
    }

    public void setLogtime(String logtime) {
        this.logtime = logtime;
    }
    
    public static String getKindString(int kind)
    {
        String status = "unknown";
        if(kind==PARKING_LOG)
        {
            status = "parking";
        }
        else if (kind == PRESENCE_LOG)
        {
            status = "presence";
        }
        return status;
    }

    @Override
    public String toString() {        
        String message;
        if(logIp==null)
        {
            message = logtime + " - "+logMessage;
        }else{
            message = logtime + " - from : "+logIp+" - "+logMessage;
        }        
        
        return message;
    }
    
    

    /**
     * Masukkan data log terbaru
     * @param con - Connect DB
     * @param log - data log yang sudah disimpan dalam object LogData
     * @throws Exception 
     */
    public static void insertLog(ConnectDB con,LogData log) throws Exception
    {
        String tableName="";
        if(log.getLogKind()==LogData.PARKING_LOG)
        {
            tableName = DB_PARKING;
        }
        else if(log.getLogKind()==LogData.PRESENCE_LOG)
        {
            tableName = DB_PRESENCE;
        }
        String sql = "INSERT INTO "+tableName+ "("+CARD_ID+","+CLIENT_MSG+","+LOG_IP+","+LOG_TIME+") "
//                + "VALUES ('"+log.getCard().getcId()+"',\""+log.getLogMessage()+"\",\""+log.getLogIp()+"\",\""+log.getLogtime()+"\")";
                + "VALUES ('"+log.getCard().getcId()+"',\""+log.getLogMessage()+"\",\""+log.getLogIp()+"\",NOW())";
        con.updateQuery(sql);
    }
    
    /**
     * Masukkan data log terbaru
     * @param con - Connect DB
     * @param log - data log yang sudah disimpan dalam object LogData
     * @throws Exception 
     */
    public static void insertLogEmergency(ConnectDB con,LogData log) throws Exception
    {
        String tableName="btp_rfid_emergency_log";
        
        String sql = "INSERT INTO "+tableName+ "(emergency_log_time,log_ip) "
                + "VALUES ("+log.getLogtime()+",\""+log.getLogIp()+"\")";
        con.updateQuery(sql);
    }
    
    /**
     * Dapatkan data-data log presensi
     * @param con
     * @return 
     */
    public static List<LogData> listPresenceLog(ConnectDB con) throws Exception
    {
        List<LogData> logs = new ArrayList<>();
        String sql = "SELECT * FROM "+DB_PRESENCE;
        con.seleksi(sql);
        while(con.getNext())
        {
            LogData log = new LogData();
            String cardId = con.getText(CARD_ID);
            
            log.setCard(Card.findById(con, cardId));
            log.setLogIp(con.getText(LOG_IP));
            log.setLogKind(PRESENCE_LOG);
            log.setLogMessage(con.getText(CLIENT_MSG));
            log.setLogtime(con.getText(LOG_TIME));
            
            logs.add(log);
        }
        return logs;
    }
}
