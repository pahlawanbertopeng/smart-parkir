/*
 * The MIT License
 *
 * Copyright 2014 elan.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package smartparking.db.model;

import java.io.ByteArrayInputStream;
import java.nio.charset.Charset;

/**
 *
 * @author elan
 */
public class ResponseData {
    private int isCard;
    private int isRegister;
    private int isOpen;
    private String datetime;
    private String userId;
    private String username;
    private String description;

    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getIsCard() {
        return isCard;
    }

    public void setIsCard(int isCard) {
        this.isCard = isCard;
    }

    public int getIsOpen() {
        return isOpen;
    }

    public void setIsOpen(int isOpen) {
        this.isOpen = isOpen;
    }

    public int getIsRegister() {
        return isRegister;
    }

    public void setIsRegister(int isRegister) {
        this.isRegister = isRegister;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public String toString() {
        //return isCard + "#" + isRegister + "#" + isOpen + "#" + datetime + "#" + userId + "#" + username + "#" + description;
        return isCard + "#" + isRegister + "#" + isOpen+"\n" ;        
    }
        
    public byte[] toBytes() throws Exception{
        byte[] combined = toString().getBytes();
        combined = toString().getBytes(Charset.forName("UTF-8"));
//        System.arraycopy(Integer.toString(isCard).getBytes("utf-8"), 0, combined, 0, 1);
//        System.arraycopy(Integer.toString(isRegister).getBytes("utf-8"), 0, combined, 1, 1);
//        System.arraycopy(Integer.toString(isOpen).getBytes("utf-8"), 0, combined, 2, 1);
//        System.arraycopy(datetime.getBytes("utf-8"), 0, combined, 3, 14);
//        System.arraycopy(userId.getBytes("utf-8"), 0, combined, 17, 10);
//        System.arraycopy(username.getBytes("utf-8"), 0, combined, 27, 10);
//        System.arraycopy(description.getBytes("utf-8"), 0, combined, 37, 10);
//        combined = Arrays.copyOfRange(Integer.toString(isCard).getBytes("utf-8"), 0, 1);
//        combined = Arrays.copyOfRange(Integer.toString(isRegister).getBytes("utf-8"), 0, 1);
//        combined = Arrays.copyOfRange(Integer.toString(isOpen).getBytes("utf-8"), 0, 1);
//        combined = Arrays.copyOfRange(datetime.getBytes("utf-8"), 0, 14);
//        combined = Arrays.copyOfRange(userId.getBytes("utf-8"), 0, 10);
//        combined = Arrays.copyOfRange(username.getBytes("utf-8"), 0, 10);
//        combined = Arrays.copyOfRange(description.getBytes("utf-8"), 0, 10);
        return combined;
    }        
    public static byte[] getByte(String message){        
        byte []msg = message.getBytes();
        msg = message.getBytes(Charset.forName("UTF-8"));
        return msg;        
    }
    
    public static String getString(byte[] message)
    {
        String msg = "";
        int i;
        if(message!=null)
        {
            ByteArrayInputStream byteInput = new ByteArrayInputStream(message);
            while((i=byteInput.read())!=-1)
            {
                msg+=(char)i;
            }
        }              
        return msg;
    }
}
