/*
 * The MIT License
 *
 * Copyright 2014 elan.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package smartparking.db.model;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import smartparking.db.ConnectDB;

/**
 *
 * @author elan
 */
public class Room 
{
    public static final String USER_ROOM_TRANS = "btp_rfid_room_transcation";
    public static final String DB_NAME = "btp_rfid_master_room";
    public static final String ROOM_ID = "room_id";
    public static final String ROOM_NAME = "room_name";
    public static final String ROOM_IP = "room_ip";
    public static final String ROOM_DESC = "room_description";
    
    private int rId;
    private String rName;
    private String rIpAddress;
    private String rDesc;    

    public void setRId(int rId){
        this.rId = rId;
    }
    
    public int getRId(){
        return rId;
    }
    
    public void setRName(String rName){
        this.rName = rName;
    }
    
    public String getRName(){
        return rName;
    }
    
    public void setRIpAddress(String rIpAddress){
        this.rIpAddress = rIpAddress;
    }
    
    public String getRIpAddress()
    {
        return rIpAddress;
    }
    
    public void setRDesc(String rDesc){
        this.rDesc = rDesc;
    }
    
    public String getRDesc()
    {
        return  rDesc;
    }
    /**
     * Dapatkan data room berdasarkan ID
     * @param con
     * @param roomId
     * @return
     * @throws Exception 
     */
    public static Room findById(ConnectDB con,int roomId) throws Exception
    {
        Room room = null;
        String sql = "SELECT * FROM "+DB_NAME+" WHERE "+ROOM_ID+"="+roomId;
        con.seleksi(sql);
        if(con.getNext())
        {
            room = new Room();
            room.setRDesc(con.getText(ROOM_DESC));
            room.setRId(Integer.parseInt(con.getText(ROOM_ID)));
            room.setRIpAddress(con.getText(ROOM_IP));
            room.setRName(con.getText(ROOM_NAME));            
            
        }
        return room;
    }
    
    public static boolean isAuthorized(ConnectDB con, String roomIp, User user) throws Exception
    {        
        boolean status = false;
        String sql = "SELECT u.user_id FROM btp_rfid_master_user u "
                    + "JOIN btp_rfid_room_transcation tr "
                    + "ON (tr.user_id = u.user_id) "
                    + "JOIN `btp_rfid_master_room` r "
                    + "ON (tr.room_id=r.room_id) WHERE u.user_id = "+user.getuId()+" "
                    + "AND r.room_ip='"+roomIp+"'";
        con.seleksi(sql);
        
        if(con.getNext())
        {
            status = true;
        }
        
        return status;
    }
    
    /**
     * Mengembalikan list room secara keseluruhan
     * @param con
     * @return
     * @throws SQLException 
     */
    public static List<Room> roomList(ConnectDB con) throws SQLException{
        List<Room> rooms = new ArrayList<>();
        String sql = "SELECT * FROM "+DB_NAME;
        con.seleksi(sql);
        while(con.getNext())
        {
            Room room = new Room();
            room.setRDesc(con.getText(ROOM_DESC));
            room.setRId(Integer.parseInt(con.getText(ROOM_ID)));
            room.setRIpAddress(con.getText(ROOM_IP));
            room.setRName(con.getText(ROOM_NAME));
            
            rooms.add(room);
        }
        return rooms;
    }
    /**
     * Mengembalikan list room yang bisa diakses oleh user
     * 
     * @param con
     * @param userId
     * @return rooms
     * @throws SQLException 
     */
    public static List<Room> userRoomList(ConnectDB con,int userId) throws SQLException
    {
        List<Room> rooms = new ArrayList<>();
        String data = "r."+ROOM_ID+",r."+ROOM_NAME+",r."+ROOM_IP+",r."+ROOM_DESC;
        String sql = "SELECT "+data+" FROM "+DB_NAME+ " r "
                + "JOIN "+USER_ROOM_TRANS+" tr "
                + "ON(r."+ROOM_ID+"=tr."+ROOM_ID+") " 
                + "JOIN "+User.DB_NAME+" u "
                + "ON(u."+User.USER_ID+"=tr."+User.USER_ID+") "
                + "WHERE u."+User.USER_ID+"="+userId;        
        con.seleksi(sql);
        while(con.getNext())
        {
            Room room = new Room();
            room.setRDesc(con.getText(ROOM_DESC));
            room.setRId(Integer.parseInt(con.getText(ROOM_ID)));
            room.setRIpAddress(con.getText(ROOM_IP));
            room.setRName(con.getText(ROOM_NAME));
            
            rooms.add(room);
        }
        return rooms;
    }
}
