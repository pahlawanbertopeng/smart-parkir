/*
 * The MIT License
 *
 * Copyright 2014 elan.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package smartparking.db;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;
import smartparking.utils.ServerSettings;

/**
 *
 * @author elan qisthi
 */

public class ConnectDB {    
    private Connection con;
    private Statement st ;
    private ResultSet rs ;
    //private DBConnectionManager conMgr;
    private HikariConfig hikariCp;
    private HikariDataSource hikariDs;
    private static ConnectDB instance;
    
    private String uname;
    private String pwd;
    private String dbname;
    private String dbhost;
    private String dbpath;
    private final int MAX_POOL_CONNECTION = 10;
    
    private ServerSettings ss; 
    
    private ConnectDB() throws Exception{                        
        ss = ServerSettings.getDefault();
        Properties dbProp = ss.getProperties();
        
        dbname = dbProp.getProperty(ServerSettings.PROP_DBNAME);
        //dbpath = dbProp.getProperty(ServerSettings.PROP_DBPATH);
        //dbhost = dbProp.getProperty(ServerSettings.PROP_DB_HOST);
        uname = dbProp.getProperty(ServerSettings.PROP_DBUNAME);
        pwd = dbProp.getProperty(ServerSettings.PROP_DBPASS);
        
        hikariCp = new HikariConfig();
        hikariCp.setMaximumPoolSize(MAX_POOL_CONNECTION);        
        hikariCp.setDataSourceClassName("com.mysql.jdbc.jdbc2.optional.MysqlDataSource");
        hikariCp.addDataSourceProperty("serverName", "localhost");
        hikariCp.addDataSourceProperty("port", "3306");
        hikariCp.addDataSourceProperty("databaseName", dbname);
        hikariCp.addDataSourceProperty("user", uname);
        hikariCp.addDataSourceProperty("password", pwd);        
        
        hikariDs = new HikariDataSource(hikariCp);
        
        con = hikariDs.getConnection();
        st = con.createStatement();
        
    } 
    public synchronized boolean isActive()
    {
        if(instance==null)
        {
            return false;
        }
        return true;
    }
    public synchronized static ConnectDB getDefault() throws Exception{
    	return instance;
    }
    public synchronized static void DBInit() throws Exception
    {
        instance = new ConnectDB();    	
    }
   public CallableStatement callSP(String s) throws SQLException{
       CallableStatement cs = con.prepareCall(s);      
       return cs;
   }
   
    public void seleksi(String s) throws SQLException{
        if(st.isClosed() || st == null)
        {
            st = con.createStatement();
        }       
        rs = st.executeQuery(s); 
    }    
     
    public void updateQuery(String s) throws SQLException{
        if(st.isClosed() || st == null)
        {
           st = con.createStatement();
        }        
         st.executeUpdate(s);
    }

    public boolean getNext() throws SQLException{     
        return rs.next();
    }

    public boolean getPrev() throws SQLException{
        return rs.previous();
    }

    public String getText(String t) throws SQLException{
        if(!rs.isClosed())
        {
            return rs.getString(t);
        }       
        return "";
    }

    public String getText(int t) throws SQLException{
        return rs.getString(t);
    }

    public int getInt(String t) throws Exception {
        if(!rs.isClosed())
        {
            return rs.getInt(t);
        }
        return 0;    	
    }
    
    public synchronized void closeDb () throws SQLException{
        //conMgr.returnConnectionToPool(con);
        hikariDs.close();
        //con.close();
        //con.close();          
    }
    
    public synchronized void setConnection() throws SQLException
    {
        this.con = hikariDs.getConnection();
    }
    
    public synchronized Connection getConnection()
    {
        return con;
    }

}
