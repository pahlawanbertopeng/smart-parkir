/*
 * The MIT License
 *
 * Copyright 2014 elan.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package smartparking.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import smartparking.utils.ServerSettings;

/**
 *
 * @author elan
 */
public class DBConnectionManager {
    private String uname;
    private String pwd;
    private String dbname;
    private String dbpath;
    private final int MAX_POOL_CONNECTION = 10;
    
    private ServerSettings ss;    
    //Vector connections = new Vector<>();
    List connections = new ArrayList<>();
    
    public DBConnectionManager() throws Exception
    {
        ss = ServerSettings.getDefault();
        Properties dbProp = ss.getProperties();
        
        dbname = dbProp.getProperty(ServerSettings.PROP_DBNAME);
        dbpath = dbProp.getProperty(ServerSettings.PROP_DBPATH);
        uname = dbProp.getProperty(ServerSettings.PROP_DBUNAME);
        pwd = dbProp.getProperty(ServerSettings.PROP_DBPASS);
        
        initialize();
    }
    
    private void initialize() throws SQLException, ClassNotFoundException
    {
        initConnectionPool();
    }
    
    private void initConnectionPool() throws SQLException, ClassNotFoundException
    {
        while(!isFull())
        {
//            connections.add;
            connections.add(createNewConnection());
        }
    }
    
    private synchronized boolean isFull()
    {
        if(connections.size() < MAX_POOL_CONNECTION)        
        {
            return false;
        }
        return true;
    }
    
    private Connection createNewConnection() throws SQLException, ClassNotFoundException
    {
        Connection connection = null;
        
        Class.forName("com.mysql.jdbc.Driver");
        connection = DriverManager.getConnection(dbpath+dbname,uname,pwd);
        
        return connection;
    }
    
    public synchronized Connection getConnectionFromPool() 
    {
        Connection connection = null;
        if(connections.size()>0)
        {
            connection = (Connection) connections.get(0);
            connections.remove(0);
        }
        return connection;
    }
    
    public synchronized void returnConnectionToPool(Connection connect)
    {
        connections.add(connect);
    }
    
}
