/*
 * The MIT License
 *
 * Copyright 2014 elan.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package smartparking;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.logging.Level;
import org.joda.time.DateTime;
import org.quartz.CronScheduleBuilder;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;
import smartparking.db.ConnectDB;
import smartparking.db.model.LogData;
import smartparking.serverhandler.ServerHandler;
import smartparking.serverlistener.ServerListener;
import smartparking.serverlistener.SockServer;
import smartparking.utils.Logging;
import smartparking.utils.ServerSettings;

/**
 *
 * @author elan
 */
public class Init {
    private static ConnectDB con;
    private static Init instance;
    private static Logging logging;   

    private static ServerSettings settings;
    private static ServerListener server;
    
    private ServerHandler sh;
    
    private static Trigger logTrigger;
    private static JobKey logJob;
    private static Scheduler logScheduler;        
    
    private String programName;
    
    protected static Thread serverThread;
    private static SockServer sockServer;
    /**
     * Inisiasi program server. Dijalankan pertama kali saat program dijalankan
     * @throws Exception 
     */
    private Init() throws Exception
    {
        LogData log = new LogData();
        // Open setting file        
        settings = ServerSettings.getDefault();                
        programName = LogData.getKindString(Integer.parseInt(ServerSettings.getPropertyValue(settings, ServerSettings.PROP_PROGRAM)));
        // Logging iniitalization        
        logging = new Logging();
        logging.setFilename(new DateTime().toString(Logging.DATE_FORMAT)+" - btp -"+ programName+"- log.log");
        logging.init();            
        
        // Open database connection        
        openDb();
        log.setLogMessage("Successfully connected to database");
        log.setLogtime(new DateTime().toString(Logging.DATE_FORMAT));
        logging.log(log.toString(), Level.CONFIG);
        
        JobDetail logDetail=null;
        // Initialize log job
        if(logJob==null)
        {
            logJob = new JobKey("Log Scheduling");        
            logDetail = JobBuilder.newJob(Logging.class)
                .withIdentity(logJob)
                .build();
        // Put the object data to log job        
        logDetail.getJobDataMap().put(ServerSettings.SETTING_NAME, settings);
        }
        
        if(logTrigger==null)
        {
        // Initialize Job Trigger
        // Dengan ekspresi cron 0 0 0 1/1 * ? * 
        // maka log file akan ditulis sehari sekali pada jam 12:00 AM        
        logTrigger = TriggerBuilder
                .newTrigger()
                .withIdentity("Log File Trigger")
                .withSchedule(
                    CronScheduleBuilder.cronSchedule("0 0 0 1/1 * ? *") 
//                  CronScheduleBuilder.cronSchedule("0 0/1 * 1/1 * ? *") 
//                --> run setiap 1 menit sekali. hanya untuk keperluan testing
                )
                .build();    
        }
        if(logScheduler==null)
        {
            // Initialize scheduler      
           logScheduler = new StdSchedulerFactory().getScheduler();
            // Attach job and trigger using scheduler
           logScheduler.start();
           logScheduler.scheduleJob(logDetail,logTrigger);
        }
        
       
       log.setLogMessage("Log Scheduler successfully initialized");
       log.setLogtime(new DateTime().toString(Logging.DATE_FORMAT));
       logging.log(log.toString(), Level.CONFIG);
       
       // Initialize server listener       
       int portNumber = Integer.parseInt(ServerSettings.getPropertyValue(settings, ServerSettings.PROP_SERVER_PORT));                     
       ServerSocket server = new ServerSocket(portNumber);
       sockServer = SockServer.handle(server);
       startServer(sockServer);
       //server = ServerListener.getDefault();          
       //startServer();
       log.setLogMessage("Server successfully initialized");
       log.setLogtime(new DateTime().toString(Logging.DATE_FORMAT));
       logging.log(log.toString(), Level.CONFIG);
       
       log.setLogMessage("Listening to any client request....");
       log.setLogtime(new DateTime().toString(Logging.DATE_FORMAT));
       logging.log(log.toString(), Level.INFO);
       
       
       
    }
    public static void serverStop()
    {
        server.stop();        
    }
    
    public static void reconnect() throws Exception
    {
       int portNumber = Integer.parseInt(ServerSettings.getPropertyValue(settings, ServerSettings.PROP_SERVER_PORT));                     
       ServerSocket server = new ServerSocket(portNumber);
       sockServer = SockServer.handle(server);
       startServer(sockServer);
    }
    
    public static void startServer(final SockServer sockServer)
    {         
        if(serverThread == null)
        {
            serverThread= new Thread(new Runnable() {
            @Override
            public void run() {
                sockServer.start();
            }
            });                   
            serverThread.start();        
        }              
    }
    
    public static ConnectDB getDBConnectionInstance()
    {
        return con;
    }
    
    public static SockServer getServerListener() {
        return sockServer;
    }   
    
    public static ServerSettings getServerInstance()
    {
        return settings;
    }
    
    public static void setLoggerInstance(Logging logging)
    {
        Init.logging = logging;
    }
    
    public static Logging getLoggerInstance()
    {
        return logging;
    }
    
    private static void openDb() throws Exception
    {
        ConnectDB.DBInit();
        con = ConnectDB.getDefault();
    }
    
    private static void closeDb() throws Exception{
        con.closeDb();
    }            
    public static void command(String cmd) throws Exception{
        switch(cmd){
            case "restart":
                reload();
                break;
            case "server-start":
                checkServer(cmd);
                break;
            case "server-stop":
                checkServer(cmd);
                break;
            case "db-start":
                //db command
                checkDb(cmd);
                break;
            case "db-stop":
                //db command
                checkDb(cmd);
                break;    
            case "status":
                //display message status
                showStatus();
                break;
            case "help":                
                // display help operation
                help();
                break;
            case "exit":
                //close the program                          
                System.exit(0);
                break;
            default:
                System.out.println("Unknown command '"+cmd+"'. Try enter 'help' for list of server commands");                
                break;
        }
    }    
    public static void showStatus()
    {
        
        boolean serverStatus=sockServer.isActive();
        boolean databaseStatus=con.isActive();
        HashMap<String,String> clientList = SockServer.getConnectedClient();                
        
        String stat = "STOPPED";
        String db = "STOPPED";
        if(serverStatus)
        {
            stat = "ACTIVE";
        }
        if(databaseStatus)
        {
            db = "ACTIVE";
        }
//        for(String s : clientAddress)
//        {
//            address+=s+"\n";
//        }
        System.out.println("Server status | "+new DateTime().toString(Logging.DATE_FORMAT)+"\n");
        System.out.println("Server : " + stat);
        System.out.println("Database : " +db);
        System.out.println("Client Status : ");
        for(Map.Entry<String,String> client : clientList.entrySet())
        {
            System.out.println(client.getValue());
        }
//        System.out.println("\t\t\tConnected Devices : "+connectedClient);
//        System.out.println("\t\t\tList devices address : "+address);
    }    
    public static void checkDb(String cmd) throws Exception
    {
        String[] regex = cmd.split("-");
        cmd = regex[1];
        switch(cmd)
        {
            case "start":                
                if(con.isActive())
                {
                    System.out.println("Database already active");                    
                }else{
                    System.out.println("Activating Database Connection...");
                    openDb();
                    System.out.println("Database successfuly activated");
                }
                break;
            case "stop":
                if(con.isActive())
                {
                    System.out.println("Stopping Database...");
                    closeDb();
                    System.out.println("Database connection successfuly stopped");
                }else{
                    System.out.println("Database is already stopped");                    
                    
                }
                break;
        }
    }    
    public static void checkServer(String cmd)
    {        
        String[] regex = cmd.split("-");
        cmd = regex[1];
        switch(cmd)
        {
            case "start":                
                if(server.isStarted())
                {
                    System.out.println("Server is already started");                    
                }else{
                    try {
                        System.out.println("Starting server...");
                        //startServer();
                        reconnect();
                        System.out.println("Server successfuly started");
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
                break;
            case "stop":
                if(server.isStarted())
                {
                    System.out.println("Stopping Server...");
                    serverStop();
                    System.out.println("Server successfuly stopped");
                }else{
                    System.out.println("Server is already stopped");                    
                    
                }
                break;
            
            default:
                break;
        }
    }   
    public static void help()
    {
        System.out.println("List of server command\n"
                + "restart\t : Restart the server, this operation will close any available database and server connection and restart them (WARNING : STILL UNSTABLE)\n "
                + "server-start\t : Start the server\n"
                + "server-stop\t : Stop the server\n"
                + "db-start\t : Start the database connection\n"
                + "db-stop\t : Stop the database connection\n"
                + "server-status\t : Display server status\n"
                + "help\t : Display this messages");
    }    
    private static void reload() throws Exception
    {
        if(instance!=null)
        {            
            logScheduler.deleteJob(logJob);   
            instance = null;
            logging = null;            
            settings = null;
            
            serverStop();
            con.closeDb();
            System.out.println("Server restarting.....");
            instance = new Init();
            System.out.println("Server successfully restarted");
        }
    }

    
    
    public static void main(String[] args) 
    {
        try {
            instance = new Init();
            Scanner scan = new Scanner(System.in);
            String input = "";
            while(true)
            {
                try{
                    input = scan.nextLine();
                    command(input);
                }catch(SQLException ex)
                {
                    try(PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("error-log.txt", true)))) {
                        //out.println(DateTime.now()+" : error "+ex.getMessage());
                        ex.printStackTrace();
                        ex.printStackTrace(out);
                    }catch (IOException e) {
                        //exception handling left as an exercise for the reader
                    }
                    if(con!=null)
                    {
                        closeDb();
                    }
                    openDb();
                }
                
                
            }
        }         
        catch (Exception ex) {               
            try(PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("error-log.txt", true)))) {                        
                
                //ex.printStackTrace();
                ex.printStackTrace(out);
                try {
                    reconnect();
                } catch (Exception ex1) {
                    ex1.printStackTrace();
                }
            }catch (IOException e) {
                try {
                    //hopefully. server error
                    
                } catch (Exception ex1) {
                    //ex1.printStackTrace();
                }
            }
        }                  
    }
}
