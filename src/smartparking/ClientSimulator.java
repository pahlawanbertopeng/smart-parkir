/*
 * The MIT License
 *
 * Copyright 2014 elan.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package smartparking;

import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.Socket;

/**
 *
 * @author elan
 */
public class ClientSimulator {
    private Socket clientSocket;
    private DataInputStream is;
    private static DataOutputStream os;
    
    public ClientSimulator(String hostname,int port) throws Exception
    {
        clientSocket = new Socket(hostname, port);
        is = new DataInputStream(clientSocket.getInputStream());
        os = new DataOutputStream(clientSocket.getOutputStream());
    }
    
    public void send(byte[] buffer) throws IOException
    {
        os.write(buffer);
        System.out.println("Message successfully sent to "+clientSocket.getLocalAddress().getHostAddress());
    }
    
    public static void main(String[] args) throws Exception{
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        try {
            String sClientType = "1#";
            String sCardId = "12345678#";
            String isCaptured = "1#";
            String sMessage = "successful";
            
            
            byte[] a = sClientType.getBytes("utf-8");
            byte[] b = sCardId.getBytes("utf-8");
            byte[] c = isCaptured.getBytes("utf-8");
            byte[] d = sMessage.getBytes("utf-8");
            byte[] combined = new byte[23];               
            
            System.arraycopy(a, 0, combined, 0, 2);
            System.arraycopy(b, 0, combined, 2, 9 );
            System.arraycopy(c, 0, combined, 11, 2);
            System.arraycopy(d, 0, combined, 13, 10);
            
           
            ClientSimulator cs = new ClientSimulator("10.130.1.58", 61232);            
            System.out.println(combined.toString());
            cs.send(combined);                                                                                                                                                     
        } catch (UnsupportedEncodingException ex) {
            ex.printStackTrace();
        } finally {
            try {
                out.flush();
                os.flush();                
                out.close();                
                os.close();
            } catch (IOException ex) {
                System.out.println(ex.getMessage());
                
            }
        }
    }
    
    class ServerListen extends Thread{
        
    }
}
