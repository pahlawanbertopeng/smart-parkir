/*
 * The MIT License
 *
 * Copyright 2014 elan.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package smartparking.serverlistener;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.logging.Level;
import org.joda.time.DateTime;
import smartparking.Init;
import smartparking.db.ConnectDB;
import smartparking.db.model.LogData;
import smartparking.db.model.RequestData;
import smartparking.db.model.ResponseData;
import smartparking.serverhandler.ServerHandler;
import smartparking.utils.Logging;

/**
 *
 * @author elan
 */
public class SockServer extends Thread {
    private static SockServer socketServer = null;
    private Socket socket;
    private ServerSocket server;
    private BufferedInputStream in;
    private BufferedOutputStream out;
    private boolean isDisconnect = false;
    private boolean isStop = false;
    //private ServerHandler sh;
    private static HashMap<String,String> clientList;
    
    public static synchronized SockServer getServerInstance()
    {
        if(socketServer!=null)
        {
            return socketServer;
        }
        return null;
    }
    
    public static synchronized HashMap<String,String> getConnectedClient()
    {
        return clientList;
    }
        
    public SockServer(ServerSocket server)
    {
        this.server = server;
        this.isDisconnect = false;
        this.isStop = false;
        clientList = new HashMap<>();
    }
    
    public synchronized void setStop(boolean flag)
    {
       isStop = flag;
       if(server!=null && flag)
       {
            try {
                server.close();
            } catch (IOException ex) {
                try(PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("error-log.txt", true)))) {
                        //out.println(DateTime.now()+" : error "+ex.getMessage());
                        ex.printStackTrace();
                        ex.printStackTrace(out);
                    }catch (IOException e) {
                        //exception handling left as an exercise for the reader
                    }
            }
       }
    }
    
    public synchronized void setDisconnect(boolean flag)
    {
        if(socket!=null && flag)
        {
            try {
                socket.close();
            } catch (IOException ex) {
                try(PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("error-log.txt", true)))) {
                        //out.println(DateTime.now()+" : error "+ex.getMessage());
                        ex.printStackTrace();
                        ex.printStackTrace(out);
                    }catch (IOException e) {
                        //exception handling left as an exercise for the reader
                    }
            }
        }
        isDisconnect = flag;
    }
    /*
     * Digunakan untuk inisiasi server secara aman
     */
    public static synchronized SockServer handle(ServerSocket s)
    {
       if(socketServer==null)
       {
           socketServer = new SockServer(s);
       }else if(socketServer!=null)
       {
           socketServer.setDisconnect(true);
           socketServer.setStop(true);
           if(socketServer.socket!=null)
           {
               socketServer.socket=null;
           }
           if(socketServer.server!=null)
           {
               socketServer.server=null;
           }
           socketServer.server = null;
           socketServer.socket = null;
           socketServer = new SockServer(s);
       }
       return socketServer;
   }
    
    public boolean isActive()
    {
       if(socketServer==null)
       {
           return false;
       }else{
           return true;
       }       
   }
    
    protected static synchronized void updateList(String key, String value)
    {
        clientList.put(key,value);
    }
    
    @Override
    public void run() {
        while(!isStop)
        {
            try {
                socket = server.accept();
            } catch (IOException ex) {
                if(!isStop)
                {
                    System.out.println("Error accepting client");
                    isStop = true;
                }
                continue;
            }
            try {
                updateList(socket.getInetAddress().getHostAddress(), "client "+socket.getInetAddress().getHostAddress()+" is connected");                                
                new SockClient(socket).start();                
            } catch (Exception ex) {                
                System.out.println(ex.getMessage());
                try(PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("error-log.txt", true)))) {
                        //out.println(DateTime.now()+" : error "+ex.getMessage());
                        //ex.printStackTrace();
                        ex.printStackTrace(out);
                    }catch (IOException e) {
                        //exception handling left as an exercise for the reader
                    }
            }
        }
    }
   
    private static String readMessage(BufferedInputStream input) throws IOException
    {            
        int i;
        String buff = new String();
        while((i=input.read())!=-1)
        {
            buff+=(char)i;
            //if(i == 0)
            //   break;
            if(i == 46)
                break;
        }
        
        if(i==-1)
        {
            return null;
        }
        //input.mark(input.available());
        //input.reset();
        //int s = input.read();        
        /*
        try 
        {
            Thread.sleep(200);
        } catch (InterruptedException ex) {
            try(PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("./error-log.txt", true)))) {
                        //out.println(DateTime.now()+" : error "+ex.getMessage());
                        ex.printStackTrace();
                        ex.printStackTrace(out);
                    }catch (IOException e) {
                        //exception handling left as an exercise for the reader
                    }
        }            
         */
         
//        if(s==-1)
//        {
//            return null;
//        }        
//        buff+= ""+(char)s;
//        int length = input.available();
//        System.out.println("Message length = "+length);
//        if(length > 0)
//        {
//            byte[] bytedata = new byte[length];
//            input.read(bytedata); 
//            buff += new String(bytedata);
//        }
        return buff;
    }
    
    protected class SockClient extends Thread
    {
        private Socket clientSocket;
        private InputStream is;
        private OutputStream os;
        private BufferedInputStream clientInput;
        private DataOutputStream clientOutput;
        //private ConnectDB dbInstance;
        
        private ServerHandler shHand;
        private LogData log;
        private final Logging logger = Init.getLoggerInstance();
        
        public SockClient(Socket clientSocket) throws IOException, Exception
        {
            if(clientSocket!=null)
            {
                this.clientSocket = clientSocket;            
                is = clientSocket.getInputStream();
                os = clientSocket.getOutputStream();
                clientInput = new BufferedInputStream(is);
                clientOutput = new DataOutputStream(os);
                shHand = new ServerHandler();

                log = new LogData();
                String ipClient = clientSocket.getInetAddress().getHostAddress();
                log.setLogMessage("device "+ipClient+" is connected");
                log.setLogtime(new DateTime().toString(Logging.DATE_FORMAT));
                logger.log(log.toString(), Level.INFO);   
                
                //dbInstance = ConnectDB.getDefault();
            }            
        }

        @Override
        public void run() {
            String receive = null;            
            while(true)
            {
                try {
                    receive = SockServer.readMessage(clientInput);
                    //check if the received data is null
                    if(receive==null)
                    {                       
                        setDisconnect(true);
                        System.out.println("client "+clientSocket.getInetAddress().getHostAddress()+" disconnected");
                        break;
                    }
                   //print the received data
                    System.out.println("[RAW]receive message from "+clientSocket.getInetAddress().getHostAddress()+" = "+receive);
                    //parse the received data into requestdata object
                    RequestData req = new RequestData(receive);
                    log.setLogMessage(req.toString());
                    log.setLogIp(clientSocket.getInetAddress().getHostAddress());
                    log.setLogtime(new DateTime().toString(Logging.DATE_FORMAT));
                    logger.log(log.toString(), Level.INFO);                    
                    System.out.println("[FORMATTED]"+clientSocket.getInetAddress().getHostAddress()+" to [SERVER]"+req.toString());
                    //check if the received data is the valid message
                    if(req.isValidMsg())
                    {
                        shHand.setSa(clientSocket.getInetAddress().getHostAddress());
                        byte [] nilaiReturn = shHand.processRequest(req,clientSocket.getInetAddress().getHostAddress());
                        clientOutput.write(nilaiReturn,0,nilaiReturn.length);
                        System.out.println("[SERVER] to "+clientSocket.getInetAddress().getHostAddress()+" "+ResponseData.getString(nilaiReturn));                                                                            
                    }
                    else
                    {
                        System.out.println("Data is not valid");                        
                        clientOutput.write(ResponseData.getByte("Data is not valid\n"));
                    }
                    
                    } catch (IOException ex) {
                        log.setLogMessage("Server throw exception : "+ex.getMessage());
                        log.setLogIp(clientSocket.getInetAddress().getHostAddress());
                        log.setLogtime(new DateTime().toString(Logging.DATE_FORMAT));
                        logger.log(log.toString(), Level.INFO);                    
                        
                        System.out.println("Server throw exception : "+ex.getMessage());
                        setDisconnect(true);
                        if(!isDisconnect)
                        {
                            log.setLogMessage("Lost Client connection");
                            log.setLogIp(clientSocket.getInetAddress().getHostAddress());
                            log.setLogtime(new DateTime().toString(Logging.DATE_FORMAT));
                            logger.log(log.toString(), Level.INFO);                    
                            System.out.println("Lost Client connection");
                        }
                        else{
                            log.setLogMessage("Forcefully close client connection");
                            log.setLogIp(clientSocket.getInetAddress().getHostAddress());
                            log.setLogtime(new DateTime().toString(Logging.DATE_FORMAT));
                            logger.log(log.toString(), Level.INFO);                    
                            System.out.println("Forcefully close client connection");
                        }                                                
                        break;
                    }
            }
            
            if(clientSocket!=null)
            {
                    try {
                        socket.close();
                    } catch (IOException ex) {  
                        //log.setLogMessage("Forcefully close client connection");
                        //log.setLogtime(new DateTime().toString(Logging.DATE_FORMAT));
                        //logger.log(log.toString(), Level.INFO);                    
                        
                        System.out.println("client "+clientSocket.getInetAddress().getHostAddress()+" forcefully disconnected");
                        System.out.println("Error : "+ex.getMessage());                        
                        
                        log.setLogMessage("Error : "+ex.getMessage()+ " \nclient "+clientSocket.getInetAddress().getHostAddress()+" forcefully disconnected");
                        log.setLogtime(new DateTime().toString(Logging.DATE_FORMAT));
                        logger.log(log.toString(), Level.INFO);                    
                        
                        try(PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("error-log.txt", true)))) {
                        //out.println(DateTime.now()+" : error "+ex.getMessage());
                        //ex.printStackTrace();
                        ex.printStackTrace(out);
                        }catch (IOException e) {
                            //exception handling left as an exercise for the reader
                        }
                    }
            }
            //update client connection status
            updateList(clientSocket.getInetAddress().getHostAddress(), "client "+clientSocket.getInetAddress().getHostAddress()+" is disconnected");            
            log.setLogMessage("device "+clientSocket.getInetAddress().getHostAddress()+" is disconnected");
            log.setLogtime(new DateTime().toString(Logging.DATE_FORMAT));
            logger.log(log.toString(), Level.INFO);   
        }           
    }
}
