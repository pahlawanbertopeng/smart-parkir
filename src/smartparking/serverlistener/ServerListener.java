/*
 * The MIT License
 *
 * Copyright 2014 elan.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * 
 * Implementasi server lama. Untuk yang terbaru dipindah di class SockServer.java
 */

package smartparking.serverlistener;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import org.joda.time.DateTime;
import smartparking.Init;
import smartparking.db.model.LogData;
import smartparking.db.model.RequestData;
import smartparking.db.model.ResponseData;
import smartparking.db.model.Room;
import smartparking.serverhandler.ServerHandler;
import smartparking.utils.Logging;
import smartparking.utils.ServerSettings;

/**
 *
 * @author elan, ian
 */
public class ServerListener {
    protected  ArrayList<ClientListener> clientListener = new ArrayList<>();
    private static final ServerSettings settings = Init.getServerInstance();
    private static ServerListener instance;
    private String ipAddress;
    private int portNumber;
    private int backlog;
    private boolean flag = true;
    private ServerHandler shHand;    
    private static List<Room> roomList;    
    
    protected ServerSocket server;
    protected InetAddress host;    
    
    
    private ServerListener() throws Exception
    {       
       ipAddress = settings.getProperties().getProperty(ServerSettings.PROP_SERVER_IP);
       portNumber = Integer.parseInt(settings.getProperties().getProperty(ServerSettings.PROP_SERVER_PORT));
       backlog = Integer.parseInt(settings.getProperties().getProperty(ServerSettings.PROP_MAX_CONNECTION));       
       shHand = new ServerHandler();
    }    
            
    public static ServerListener getDefault() throws Exception
    {
        if(instance==null)
        {
            instance = new ServerListener();
        }
        return instance;
    }
    
    public boolean isStarted()
    {
        return flag;
    }
    
    public int getConnectedClient()
    {
        return clientListener.size();
    }
    
    public List<String> getClientAddress()
    {
        List<String> clientAddress = null;
        for(ClientListener cl : clientListener)
        {
            clientAddress.add(cl.sock.getInetAddress().getHostAddress());
        }
        return clientAddress;
    }
    
    /**
     * Start server listener
     */
    public void start(){                
        boolean isAllowed =false;
        flag=true;
        LogData log;
        Logging logger = Init.getLoggerInstance();
        try {
            //host = InetAddress.getByName(ipAddress);
            server = new ServerSocket(portNumber);            
            log = new LogData();
            log.setLogMessage("Server listen to client within the port :"+portNumber);
            ClientListener cl;            
            while(flag)
            {                    
                log.setLogtime(new DateTime().toString(Logging.DATE_FORMAT));
                logger.log(log.toString(), Level.INFO);                                                        
                if(!flag)
                {
                    break;
                }   
                //update room list
                //updateRoomList();                
                // Accept incoming connection and write it to socket object                
                Socket sock = server.accept();         
                String clientAddress = sock.getInetAddress().getHostAddress();
                System.out.println(clientAddress);
                for(Room room : Room.roomList(Init.getDBConnectionInstance()))
                {
                    if(clientAddress.equalsIgnoreCase(room.getRIpAddress()))
                    {                  
                        isAllowed = true;                                                                        
                        break;
                    }
                }
                System.out.println("Is Allowed : "+isAllowed);
                if(isAllowed)
                {
                   //This client is permitted to connect with our server                    
                    isAllowed = false;                                        
                    cl = new ClientListener(sock);
                    clientListener.add(cl);                                              
                    cl.start();                            
                }else{
                    //This client not allowed connect to server. Disconnect it                    
                    //log.setLogMessage("Client "+clientAddress+" is not allowed to connect" );                    
                    sock.close();
                    sock=null;                                               
                }                
            }   
            //stop the server
            stop();
        } catch (Exception ex) {            
            ex.getMessage();
        }         
    }
    
    public void stop()
    {
        LogData log;
        if(instance!=null && flag)
        {
            try {
                //disconnect any connected clients first
                for(ClientListener cl : clientListener)
                {                    
                    cl.close();
                    cl=null;
                }
                
                //stop server
                System.out.println("server stop true");
                flag = false;
                server.close();                           
                System.out.println("server socket closed");
                log = new LogData();
                log.setLogMessage("Server closed");
                
            } catch (IOException ex) {
                ex.getMessage();
            }
        }
    }
    
    protected class ClientListener extends Thread{
        private Socket sock;
        private DataInputStream inputStream;
        private DataOutputStream outputStream;        
        private LogData log;
        private final Logging logger = Init.getLoggerInstance();
        private boolean clientflag = true;
        private String ipClient;
        
        private String verboseStatus;
        
        
        public ClientListener(Socket sock) throws Exception
        {            
            this.sock = sock;            
            inputStream = new DataInputStream(sock.getInputStream());
            outputStream = new DataOutputStream(sock.getOutputStream());                        
            if(settings!=null)
            {
                verboseStatus = settings.getProperties().getProperty(ServerSettings.PROP_VERBOSE_MODE);
                System.out.println("verboseStatus = "+verboseStatus);
            }
            
            
            log = new LogData();
            ipClient = sock.getInetAddress().getHostAddress();
            log.setLogMessage("device "+ipClient+" is connected");
            log.setLogtime(new DateTime().toString(Logging.DATE_FORMAT));
            logger.log(log.toString(), Level.INFO);
        }
        /**
         * 
         * @param message - pesan respon yang ingin disampaikan ke client
         * @throws Exception 
         */
        public void sendMessage(byte[] buffer) throws Exception
        {            
                outputStream.write(buffer,0,buffer.length);                            
        }
        
        private void writeToConsole(byte[] buffer) throws Exception
        {                        
            int i;
            String message = "";                    
            ByteArrayInputStream byteInput = new ByteArrayInputStream(buffer);
            while((i=byteInput.read())!=-1)
            {                                    
                 message+=(char)i;
                 if(i==10)
                {
                    break;
                }
            }
            System.out.print("[VERBOSE]"+ipClient+" to [SERVER] : "+message);
        }
        
        public void close()
        {
            try {
                inputStream.close();
                sock.close();
                System.out.println("client socket thread closed");
            } catch (IOException ex) {
                ex.getMessage();
            }
        }                
        
        /**
         * Jalankan thread client listener setiap ada request masuk dari user
         */
        @Override
        public void run() {            
            try {                                
                byte[] buffer = new byte[64];                 
                int i;
                while(flag)
                {                           
                    if(!clientflag)
                    {
                        break;
                    }                     
                      
                    inputStream.read(buffer);
                    //wait until data is being inputed
                    Thread.sleep(500);
                    if(buffer!=null)
                    {                                                                      
                            ByteArrayInputStream msgClientFull = new ByteArrayInputStream(buffer);
                            RequestData req = new RequestData("");
                            //Verbose mode
                            if (verboseStatus.equals("1")) {
                                writeToConsole(buffer);
                                System.out.println("[VERBOSE]Message length is : "+(req.length()-3));
                            }                            
                            log.setLogMessage(req.toString());
                            log.setLogIp(ipClient);
                            log.setLogtime(new DateTime().toString(Logging.DATE_FORMAT));
                            logger.log(log.toString(), Level.INFO);
                                
                            System.out.println("[FORMATTED]"+ipClient+" to [SERVER]"+req.toString());
                            
                            if(req.isValidMsg())
                            {        
                                shHand.setSa(sock.getInetAddress().getHostAddress());
                                byte [] nilaiReturn = shHand.processRequest(req,ipClient);
                                System.out.println("[SERVER] to "+ipClient+" "+ResponseData.getString(nilaiReturn));
                                sendMessage(nilaiReturn);                               
                            }
                            else
                            {   
                                System.out.println("Data is not valid");
                                sendMessage(ResponseData.getByte("Data is not valid\n"));
                            }
                        
                    }else
                    {
                        System.out.println("Request data not found\n");                        
                    }                    
                }                
                close();
            }  catch (Exception ex) {     
                ex.printStackTrace();
                LogData log = new LogData();
                log.setLogMessage("Device with IP : "+sock.getInetAddress().getHostAddress()+" is disconnected");
                log.setLogtime(new DateTime().toString(Logging.DATE_FORMAT));                
                logger.log(log.toString(), Level.OFF);
                clientflag = false;
            }            
        }        
    }
}
